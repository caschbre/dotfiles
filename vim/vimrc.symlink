set nocompatible
set t_Co=256
" Always edit in utf-8:
set encoding=utf-8

"""""""""""""""""""""""""""""""""""""""
" Initialize plugins
"""""""""""""""""""""""""""""""""""""""

call plug#begin('~/.vim/plugged')
Plug 'airblade/vim-gitgutter'
Plug 'arnaud-lb/vim-php-namespace'
Plug 'cakebaker/scss-syntax.vim'
Plug 'csscomb/vim-csscomb'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'elzr/vim-json'
Plug 'ervandew/supertab'
Plug 'google/vim-searchindex'
Plug 'groenewege/vim-less'
Plug 'hail2u/vim-css3-syntax'
Plug 'jiangmiao/auto-pairs'
Plug 'joonty/vdebug'
Plug 'majutsushi/tagbar'
Plug 'morhetz/gruvbox'
Plug 'mustache/vim-mustache-handlebars'
Plug 'ntpeters/vim-better-whitespace'
Plug 'othree/html5.vim'
Plug 'pangloss/vim-javascript'
Plug 'prettier/vim-prettier', { 'do': 'yarn install', 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'html', 'json', 'graphql', 'markdown', 'vue'] }
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/syntastic'
Plug 'shawncplus/phpcomplete.vim'
Plug 'SuperSimen/vim-twig'
Plug 'szw/vim-tags'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-cucumber'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-obsession'
Plug 'tpope/vim-surround'
Plug 'vim-airline/vim-airline'
" Plug 'git://drupalcode.org/project/vimrc.git', {'branch': '8.x-1.x', 'rtp': 'bundle/vim-plugin-for-drupal/'}
Plug 'https://git.drupal.org/project/vimrc.git', { 'branch': '8.x-1.x', 'rtp': 'bundle/vim-plugin-for-drupal' }
call plug#end()

"""""""""""""""""""""""""""""""""""""""
" Settings for plugins
"""""""""""""""""""""""""""""""""""""""

" Autocomplete settings.
autocmd FileType php setlocal omnifunc=phpcomplete#CompletePHP
set completeopt=longest,menuone
let g:SuperTabDefaultCompletionType = "<c-x><c-o>"

" NERDTree settings.
let NERDTreeShowHidden=1
autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Syntastic settings.
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_php_phpcs_args="--standard=Drupal,DrupalPractice --extensions='php,module,inc,install,test,profile,theme,js,css,info,txt' --ignore='*.features.*.inc'"
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

" Vim Airline settings.
set laststatus=2
" Turn on Vim Airline tabs.
let g:airline#extensions#tabline#enabled = 1
"let g:airline_theme='simple'
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'

" vim-php-namespace settings.
function! IPhpInsertUse()
  call PhpInsertUse()
  call feedkeys('a',  'n')
endfunction
autocmd FileType php inoremap <Leader>u <Esc>:call IPhpInsertUse()<CR>
autocmd FileType php noremap <Leader>u :call PhpInsertUse()<CR>

" Vdebug settings.
let g:vdebug_options = {}
let g:vdebug_options['break_on_open'] = 0
let g:vdebug_options['max_children'] = 128
let g:vdebug_options['watch_window_style'] = 'compact'
" Need to set as empty for this to work with Vagrant boxes.
let g:vdebug_options['server'] = ""

" CSScomb settings.
" Automatically comb your CSS on save.
autocmd BufWritePre,FileWritePre *.css,*.less,*.scss,*.sass silent! :CSScomb

" Vim-Tags settings.
set exrc
set secure

" Prettier.
let g:prettier#exec_cmd_async = 1

"""""""""""""""""""""""""""""""""""""""
" Text editing and vim tweaks
"""""""""""""""""""""""""""""""""""""""

set noswapfile
colorscheme gruvbox
set background=dark
let g:gruvbox_contrast_dark='dark'
syntax enable

set number
set title
set visualbell
set backspace=eol,start,indent
"set nowrap

" Allow project specific .vimrc files.
set exrc
set secure

" Tab settings.
set expandtab
set shiftwidth=2
set softtabstop=2
set autoindent
set smartindent

" Folding settings.
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=1

" Open new splits on to bottom and right.
set splitbelow
set splitright

" Highlight search terms.
set hlsearch
set incsearch

" Start scrolling after the cursor has made it to the 3rd line above the bottom.
set scrolloff=3

" Show cursorline in the active vim window.
augroup CursorLine
  au!
  au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  au WinLeave * setlocal nocursorline
augroup END

" Wildmenu settings.
if has("wildmenu")
  set wildignore+=*.a,*.o
  set wildignore+=*.bmp,*.gif,*.ico,*.jpg,*.png
  set wildignore+=.DS_Store,.git,.hg,.svn
  set wildignore+=*~,*.swp,*.tmp
  set wildmenu
  set wildmode=longest,list
endif

" Automatically reload vimrc when it's saved.
augroup VimrcSo
  au!
  autocmd BufWritePost $MYVIMRC so $MYVIMRC
augroup END

" Retain buffers and allow new files to be open... all while retaining undo's.
set hidden
" Allow cursor to wrap lines when navigating sideways.
set whichwrap+=<,>,h,l,[,]
" Show matching parenthesis
set showmatch

" Turn off autohide of quotes in json-vim.
let g:vim_json_syntax_conceal = 0

" Increase speed of ctrlp by excluding files.
" https://medium.com/a-tiny-piece-of-vim/making-ctrlp-vim-load-100x-faster-7a722fae7df6
" let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
let g:ctrlp_working_path_mode = 'ra'
" Sane Ignore For ctrlp
" https://github.com/kien/ctrlp.vim/issues/313
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\.git$\|\.hg$\|\.svn$\|\.yardoc\|release\|tmp$',
  \ 'file': '\.exe$\|\.so$\|\.dat$'
  \ }

" Remove default old line.
set noshowmode

set fileformats=unix        "Use Unix line endings
set linebreak               "Break lines when appropriate

" Git Gutter
set signcolumn=yes
let g:gitgutter_eager = 0

" Bracketed Paste Workaround
" https://github.com/vim/vim/issues/1671#issuecomment-299258728
if has("unix")
  let s:uname = system("echo -n \"$(uname)\"")
  if !v:shell_error && s:uname == "Linux"
    set t_BE=
  endif
endif


"""""""""""""""""""""""""""""""""""""""
" Key mappings
"""""""""""""""""""""""""""""""""""""""

set pastetoggle=<F2>

" NERDTree mappings.
map <C-n> :NERDTreeToggle<CR>

" https://stackoverflow.com/questions/1416572/vi-vim-restore-opened-files
map <F9> :mksession! ~/.vimsession <cr>
map <F10> :source ~/.vimsession <cr>

" @todo needs ctags?
" nmap <F8> :TagbarToggle<CR>
